import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailIngredPage } from './detail-ingred.page';

describe('DetailIngredPage', () => {
  let component: DetailIngredPage;
  let fixture: ComponentFixture<DetailIngredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailIngredPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailIngredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
