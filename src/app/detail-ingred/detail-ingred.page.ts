import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-detail-ingred',
  templateUrl: './detail-ingred.page.html',
  styleUrls: ['./detail-ingred.page.scss'],
})
export class DetailIngredPage implements OnInit {
  idOb = null;
  meals: any[] =[];
  url: String ='https://www.themealdb.com/images/ingredients/';
  formato: String = '.png';

  constructor(private http: HttpClient, private activeRoute: ActivatedRoute, private navCtrl: NavController) { }

  ngOnInit() : void{
    this.idOb = this.activeRoute.snapshot.paramMap.get('id');
    console.log(this.idOb);
    this.getMeal();
  }

  getMeal(){
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/filter.php?i='+this.idOb )
      .subscribe((res : any) => {
        this.meals = res.meals;
        console.log("meals:"+this.meals);
      });
    });
  }

  goDetails(id: any){
    this.navCtrl.navigateForward(['/detail-comida',id]);
  }
}
