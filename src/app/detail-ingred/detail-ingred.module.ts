import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailIngredPageRoutingModule } from './detail-ingred-routing.module';

import { DetailIngredPage } from './detail-ingred.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailIngredPageRoutingModule
  ],
  declarations: [DetailIngredPage]
})
export class DetailIngredPageModule {}
