import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailIngredPage } from './detail-ingred.page';

const routes: Routes = [
  {
    path: '',
    component: DetailIngredPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailIngredPageRoutingModule {}
