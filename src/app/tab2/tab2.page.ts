import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  comidas: any[] =[];
  categorias: any[] =[];

  constructor(private http: HttpClient, private router: Router, private navCtrl: NavController) {}
  ngOnInit(): void {
    this.getComInicio();
    this.getCategorias();
  }
  
  getCategorias(){
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/categories.php')
      .subscribe((res : any) => {
        this.categorias = res.categories;
        console.log(this.categorias);
      });
    });
  }

  getComInicio(){
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/filter.php?c=beef' )
      .subscribe((res : any) => {
        this.comidas = res.meals;
        console.log(this.comidas);
      });
    });
  }

  cambiarCategoria(event){
    this.comidas=[];
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/filter.php?c='+event.detail.value )
      .subscribe((res : any) => {
        this.comidas = res.meals;
        console.log(this.comidas);
      });
    });
  }
  
  goDetails(id: any){
    this.navCtrl.navigateForward(['/detail-comida',id]);
  }
}
