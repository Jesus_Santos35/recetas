import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  comidas: any[] =[];

  constructor(private http: HttpClient, private router: Router, private navCtrl: NavController) {}
  ngOnInit(): void {
    this.getComInicio();
  }

  getComInicio(){
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/search.php?f=a' )
      .subscribe((res : any) => {
        this.comidas = res.meals;
        console.log(this.comidas);
      });
    });
  }

  cambiarLetra(event){
    this.comidas=[];
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/search.php?f='+event.detail.value )
      .subscribe((res : any) => {
        this.comidas = res.meals;
        console.log(this.comidas);
      });
    });
  }
  
  goDetails(id: any){
    this.navCtrl.navigateForward(['/detail-comida',id]);
  }
}
