import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  ingredientes: any[] =[];
  url: String ='https://www.themealdb.com/images/ingredients/';
  formato: String = '.png';

  constructor(private http: HttpClient, private router: Router, private navCtrl: NavController) {}
  ngOnInit(): void {
    this.getIngInicio();
  }

  getIngInicio(){
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/list.php?i=list' )
      .subscribe((res : any) => {
        this.ingredientes = res.meals;
        console.log(this.ingredientes);
      });
    });
  }

  goDetailsIn(id: any){
    this.navCtrl.navigateForward(['/detail-ingred',id]);
  }
}
