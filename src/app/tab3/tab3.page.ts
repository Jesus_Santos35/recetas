import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  comidas: any[] =[];

  constructor(private http: HttpClient, private router: Router, private navCtrl: NavController) {}
  ngOnInit(): void {
    this.getComInicio();
  }

  getComInicio(){
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/filter.php?a=American' )
      .subscribe((res : any) => {
        this.comidas = res.meals;
        console.log(this.comidas);
      });
    });
  }

  cambiarRegion(event){
    this.comidas=[];
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/filter.php?a='+event.detail.value )
      .subscribe((res : any) => {
        this.comidas = res.meals;
        console.log(this.comidas);
      });
    });
  }

  goDetails(id: any){
    this.navCtrl.navigateForward(['/detail-comida',id]);
  }
}