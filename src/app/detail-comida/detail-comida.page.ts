import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-detail-comida',
  templateUrl: './detail-comida.page.html',
  styleUrls: ['./detail-comida.page.scss'],
})
export class DetailComidaPage implements OnInit {
  idOb = null;
  details: any[] =[];
  url: String ='https://www.themealdb.com/images/ingredients/';
  formato: String = '.png';

  constructor(private http: HttpClient, private activeRoute: ActivatedRoute, private navCtrl: NavController) { }

  ngOnInit() : void{
    this.idOb = this.activeRoute.snapshot.paramMap.get('id');
    console.log(this.idOb);
    this.getMeal();
  }

  getMeal(){
    return new Promise(res => {
      this.http.get('https://www.themealdb.com/api/json/v1/1/lookup.php?i='+this.idOb )
      .subscribe((res : any) => {
        this.details = res.meals[0];
        console.log(res.meals);
        console.log(this.details);
      });
    });
  }

  goDetailsIn(id: any){
    this.navCtrl.navigateForward(['/detail-ingred',id]);
  }
}
