import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailComidaPage } from './detail-comida.page';

describe('DetailComidaPage', () => {
  let component: DetailComidaPage;
  let fixture: ComponentFixture<DetailComidaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailComidaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailComidaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
