import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailComidaPageRoutingModule } from './detail-comida-routing.module';

import { DetailComidaPage } from './detail-comida.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailComidaPageRoutingModule
  ],
  declarations: [DetailComidaPage]
})
export class DetailComidaPageModule {}
